import csv
from helpers import *

# Read location data
def readLoc(fileName):
    len=sum(1 for line in open(fileName))-1
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i=-1
        t=np.zeros(len)
        loc=np.zeros(len)
        for row in spamreader:
            if i>=0:
                t[i]=float(row[0])
                loc[i]=float(row[1])*1000/25.4
            i=i+1
    return (t,loc,len)

# Read IMU data
def readIMU(fileName):
    len=sum(1 for line in open(fileName))-1
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i=-1
        t=np.zeros(len)
        pitch=np.zeros(len)
        roll=np.zeros(len)
        for row in spamreader:
            if i>=0:
                t[i]=float(row[0])
                pitch[i]=float(row[2])
                roll[i]=float(row[1])
            i=i+1
    return (t,pitch,roll,len)

# Read Lidar data
def readLidar(fileName):
    len=sum(1 for line in open(fileName))-1
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i=-1
        t=np.zeros(len)
        lidar=np.zeros([len,360],dtype='float')
        for row in spamreader:
            if i>=0:
                t[i]=t[i]=float(row[3])+float(row[4])/10**9
                lidar_str=row[-720:-360]
                for j in range(360):
                    lidar[i][j]=float(lidar_str[j])*1000/25.4
            i=i+1
    return (t,lidar,len)

# Read centered.csv
def readCentered(fileName):
    len=sum(1 for line in open(fileName))-1
    with open(fileName,'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i=-1
        t=np.zeros(len)
        lidar=np.zeros([len,360],dtype='float')
        angle=np.zeros([len,360],dtype='float')
        for row in spamreader:
            if i>=0:
                t[i]=t[i]=float(row[0])+float(row[1])/10**9
                lidar_str=row[-720:-360]
                angle_str=row[-360:722]
                for j in range(360):
                    lidar[i][j]=float(lidar_str[j])*1000/25.4
                    angle[i][j]=float(angle_str[j])
            i=i+1
    return (t,lidar,angle,len)

# get reasonable range values from lidar
def lidarVar(lidar,bound):
    set=np.zeros(len(lidar)*360)
    cnt=0
    for i in range(len(lidar)):
        for j in range(360):
            if lidar[i,j]<bound:
                set[cnt]=lidar[i,j]
                cnt=cnt+1
    return set[0:cnt]
