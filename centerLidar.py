import numpy as np
from helpers import *
import matplotlib.pyplot as plt

def centerLidar(dataInput,params=None,testingMode=0,lidarId=0):
    (tLidar,lidar,pipeDiam,tIMU,roll,pitch,tLoc,loc)=dataInput

    mask=np.array([2,2,2,2,3,2,2,2,2],dtype='float')/19
    ef=1.3 # Expanding factor for pipeDiam
    thres=2 # When to start integrating
    sf=50 # Scaling factor between roll integral and horinzontal centering offset
    paramsDefault=(mask,ef,thres,sf)
    (mask,ef,thres,sf)=paramsHandler(params,paramsDefault)

    # Process data
    locTP=findLocTP(tLoc,loc)
    lidarTP=findRgTP(tLidar,tLoc,locTP)
    lenLidar=len(tLidar)
    pitch=sync(tLidar,tIMU,pitch)
    roll=sync(tLidar,tIMU,roll)
    roll=np.convolve(roll,mask,'same')

    # Get horinzontal and vertical centering error, meanwhile flag starts
    startFlag=0
    vOffset=np.zeros(lenLidar)
    hOffset=np.zeros(lenLidar)
    vSum=0
    vN=0
    hSum=0
    hN=0
    for i in range(1,lenLidar):
        if lidar[i,0]+lidar[i,180]<pipeDiam*ef:
            vOffset[i]=(lidar[i,0]-lidar[i,180])
            vSum=vSum+vOffset[i]
            vN=vN+1
        if lidar[i,90]+lidar[i,270]<pipeDiam*ef:
            hOffset[i]=lidar[i,90]-lidar[i,270]
            hSum=hSum+hOffset[i]
            hN=hN+1
            endP=i # Currently only correct reverse run, so this startPoint doesn't matter #if startFlag==0 and abs(roll[i])<thres: startFlag=1 lidarTP[0]=i
    if endP<lidarTP[3]:
        lidarTP[3]=endP

    # Normalize to minimize integral drifting
    roll[lidarTP[2]:lidarTP[3]]=roll[lidarTP[2]:lidarTP[3]]-sum(roll[lidarTP[2]:lidarTP[3]])/(lidarTP[3]-lidarTP[2]) #roll[lidarTP[0]:lidarTP[1]]=roll[lidarTP[0]:lidarTP[1]]-sum(roll[lidarTP[0]:lidarTP[1]])/(lidarTP[1]-lidarTP[0])

    # Integrate roll
    iRoll=np.zeros(lenLidar)
    iRoll[0]=roll[0] # Currently only correct reverse run #for i in range(lidarTP[0],lidarTP[2]): iRoll[i]=iRoll[i-1]-roll[i]*0.1#*(hOffset[i]!=0)
    for i in range(lidarTP[2],lidarTP[3]):
        iRoll[i]=iRoll[i-1]+roll[i]
    iRoll=iRoll/sf

    # Center lidar
    centeredLidarRange=np.zeros([lenLidar,360],dtype='float')
    centeredLidarAngle=np.zeros([lenLidar,360],dtype='float')
    if testingMode==0 or testingMode==1:
        for i in range(lenLidar):
            (centeredLidarRange[i],centeredLidarAngle[i])=centerLidarSlice((vSum/vN/2,(hSum/hN+iRoll[i])/2,roll[i]),lidar[i])
    else:
        i=lidarId
        (centeredLidarRange[i],centeredLidarAngle[i])=centerLidarSlice((vSum/vN/2,(hSum/hN+iRoll[i])/2,roll[i]),lidar[i])


    ''' # Return result '''
    if testingMode:
        print vSum/vN/2
        return (centeredLidarRange,centeredLidarAngle,vOffset,hOffset,iRoll)
    else:
        return (centeredLidarRange,centeredLidarAngle)


# Center Lidar data
def centerLidarSlice(offsets,lidarSlice): #lidarSlice starts from pipe top and go clockwise facing robot's direction
    (v,h,a)=offsets # PipeCenter above lidar: verticalOffset>0;
                    # PipeCenter on the right of lidar: horizontalOffset>0;
                    # LidarSlice starts before pipe top: angleOffset>0, (rollAngleFromIMU>0)
    rg=np.zeros(360,dtype='float')
    alp=np.zeros(360,dtype='float')
    for i in range(360):
        tht=(i-a)*0.01745329251 # this number is: pi*2/360
        m=lidarSlice[i]*np.sin(tht)-h
        n=lidarSlice[i]*np.cos(tht)-v
        rg[i]=np.sqrt(m**2+n**2)
        alp[i]=np.arctan(m/n)+np.pi/2* ( (m<0 and n>0)*4 + (n<0)*2 )

    return (rg,alp)

# Find location turning points from loc
def findLocTP(t,loc):
    i=0
    j=0
    TP=np.array([0]*4)
    s=0.1#0.4
    while TP[3]==0:
        step=loc[i+1]-loc[i]
        if (j==0 and step>s)|(j==1 and step<s)|(j==2 and step<-s)|(j==3 and step>-s):
            TP[j]=i
            j=j+1
        i=i+1
    TP[1]=TP[1]-1
    TP[3]=TP[3]-1
    return TP

# Find location turning points for DT50
def findRgTP(tRg,tLoc,locTP):
    i=0
    j=0
    rgTP=np.array([0]*4)
    while rgTP[3]==0:
        if tRg[i]>tLoc[locTP[j]]:
            rgTP[j]=i
            j=j+1
        i=i+1
    rgTP[1]=rgTP[1]-1
    rgTP[3]=rgTP[3]-1
    return rgTP
