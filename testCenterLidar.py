import numpy as np
import sys
import matplotlib.pyplot as plt
from helpers import *
from funcDef import *
from centerLidar import *


# Main program starts:
path='/home/dreamer/Documents/bags/HotTestBags/'#'/Users/timeaglezhao/Documents/bags/'#

bagname=['07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-09-49-20',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-10-14-12',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-001_2018-07-26-10-30-09',\
    '07-26_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-BL-002_2018-07-26-10-54-33',\
    '07-25_HotRuns/RadPiper-01_0253_30SteelPipe_2018-08-24-16-31-14',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-12-57',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-36-29',\
    '07-25_HotRuns/RadPiper-02_0254_X-333-3304-10-00-02-AL-001_2018-07-25-11-54-29',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-17-50',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-29-17',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-10-39-45',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-17-48',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-32-36',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-14-48-51',\
    '07-24_HotRuns/RadPiper-02_0254_X-333-3304-01-08-02-BL-0000000003_2018-07-24-15-04-39'
    ]
num=int(sys.argv[1])
path='/home/dreamer/Documents/bags/TestingJoshua/RadPiper-01_0253_30LongPipe_2018-10-20-19-40-49'#path+bagname[num]#
loc_csv='/.loc_log/optimized_traj.csv'
lidar_csv='/rplidar_scan.csv'
imu_csv='/smooth_imu.csv'
(tLoc,loc,lenLoc)=readLoc(path+loc_csv)
(tLidar,lidar,lenLidar)=readLidar(path+lidar_csv)
(tIMU,pitch,roll,lenIMU)=readIMU(path+imu_csv)
if num>=4 and num<=7:
    pipeDiam=42
else:
    pipeDiam=30

testingMode=2000

(centeredLidarRange,centeredLidarAngle,vOffset,hOffset,iRoll)=centerLidar((tLidar,lidar,pipeDiam,tIMU,roll,pitch,tLoc,loc),None,testingMode)

if 1:
#    plt.plot(tLidar,(vOffset),'b.',label='v-offset')
    plt.plot(tLidar,(hOffset),'g.',label='h-offset')
    #plt.plot(tLidar,(hOffset-iRoll),'r.',label='h-offset')
    plt.plot(tIMU,roll,'c.-',label='roll')
    plt.plot(tLidar,iRoll,'k.',label='roll integral')
    #plt.plot(tLidar[lidarTP],[0]*4,'ro')
    #plt.plot(tLoc,loc,'b.')
    plt.ylabel('Angle (degree)')
    plt.xlabel('Time (sec)')
    plt.legend()
    plt.show()

if 1:
    ax = plt.subplot(121,projection='polar')
    theta=np.array(range(360),dtype='float')/360*2*np.pi
    for i in range(testingMode,lenLidar,999999*testingMode+1):
        ax.plot(theta, lidar[i],'b.-')
    ax.set_theta_direction(-1)
    ax.set_theta_offset(np.pi/2.0)
    ax.set_title("Uncentered", va='bottom')

    ax = plt.subplot(122,projection='polar')
    for i in range(testingMode,lenLidar,999999*testingMode+1):
        ax.plot(centeredLidarAngle[i],centeredLidarRange[i],'r.')
    ax.set_theta_direction(-1)
    ax.set_theta_offset(np.pi/2.0)
    ax.set_title("Centered with Roll", va='bottom')
    plt.show()

if 0:
    for i in range(0,lenLidar):
        plt.plot(range(360), lidar[2000],'b')
